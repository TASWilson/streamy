import React from "react";
import { Field, reduxForm } from "redux-form";

class StreamForm extends React.Component<any> {
  renderError = ({ invalid, error, touched }: any) => {
    if (touched && invalid)
      return (
        <div className="ui error message">
          <div className="header">{error}</div>
        </div>
      );
  };

  renderInput = ({ input, label, meta }: any) => {
    const className = `field ${meta.invalid && meta.touched ? "error" : ""}`;
    return (
      <div className={className}>
        <label>{label}</label>
        <input {...input} />
        {this.renderError(meta)}
      </div>
    );
  };

  handleFormSubmit = (formValues: any) => {
    this.props.onSubmit(formValues);
  };

  render() {
    return (
      <form
        onSubmit={this.props.handleSubmit(this.handleFormSubmit)}
        className="ui form error"
      >
        <Field label="Enter Title" name="title" component={this.renderInput} />
        <Field
          label="Enter Description"
          name="description"
          component={this.renderInput}
        />
        <button className="ui button primary"> Submit</button>
      </form>
    );
  }
}

const validate = (formValues: any) => {
  const errors: any = {};
  if (!formValues.title) errors.title = "You must enter a title";
  if (!formValues.description)
    errors.description = "You must enter a description";
  return errors;
};

export default reduxForm({ form: "streamForm", validate })(StreamForm);
