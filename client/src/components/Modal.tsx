import React from "react";
import ReactDOM from "react-dom";

interface IModalProps {
  title: String | JSX.Element | null;
  content: String | JSX.Element | null;
  actions: String | JSX.Element | null;
  onDismiss(): any;
}

const Modal = (props: IModalProps) => {
  return ReactDOM.createPortal(
    <div onClick={props.onDismiss} className="ui dimmer modals visible active">
      <div
        onClick={(e) => {
          e.stopPropagation();
        }}
        className="ui standard modal visible active"
      >
        <div className="header">{props.title}</div>
        <div className="content">{props.content}</div>
        <div className="actions">{props.actions}</div>
      </div>
    </div>,
    document.querySelector("#modal") as Element
  );
};

export default Modal;
