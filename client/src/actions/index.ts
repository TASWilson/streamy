import { ThunkDispatch } from 'redux-thunk';

import {
    SIGN_IN,
    SIGN_OUT,
    CREATE_STREAM,
    EDIT_STREAM,
    FETCH_STREAM,
    FETCH_STREAMS,
    DELETE_STREAM
} from './types'
import streams from '../apis/streams';
import history from '../history'


export const createStream = (streamValue: { title: '', description: '' }) => async (dispatch: any, getState: any) => {
    const response = await streams.post('/streams', { ...streamValue, userId: getState().auth.id })
    dispatch({ type: CREATE_STREAM, payload: response.data })

    history.push('/')
}

export const fetchStreams = () => {
    return async (dispatch: ThunkDispatch<any, any, any>) => {
        const response = await streams.get('/streams')
        dispatch({ type: FETCH_STREAMS, payload: response.data })
    }
}

export const fetchStream = (id: String) => {
    return async (dispatch: ThunkDispatch<any, any, any>) => {
        const response = await streams.get(`/streams/${id}`)
        dispatch({ type: FETCH_STREAM, payload: response.data })
    }
}

export const editStream = (id: String, streamValue: any) => {
    return async (dispatch: ThunkDispatch<any, any, any>) => {
        const response = await streams.patch(`/streams/${id}`, streamValue)
        dispatch({ type: EDIT_STREAM, payload: response.data })

        history.push('/')
    }
}

export const deleteStream = (id: String) => {
    return async (dispatch: ThunkDispatch<any, any, any>) => {
        await streams.delete(`/streams/${id}`)
        dispatch({ type: DELETE_STREAM, payload: id })

        history.push("/");
    }
}

export const signIn = (id: String) => {
    return { type: SIGN_IN, payload: { id } }
}

export const signOut = () => {
    return { type: SIGN_OUT }
}