import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { fetchStreams } from "../../actions";

class StreamList extends React.Component<{
  fetchStreams(): any;
  streams: any;
  currentUserId: String;
  isSignedIn: boolean;
}> {
  componentDidMount() {
    this.props.fetchStreams();
  }

  renderAdmin(id: any, userId: any) {
    if (userId === this.props.currentUserId) {
      return (
        <div className="right floated content">
          <Link to={`/streams/edit/${id}`} className="ui button primary">
            Edit
          </Link>
          <Link to={`/streams/delete/${id}`} className="ui button negative">
            Delete
          </Link>
        </div>
      );
    } else {
      return null;
    }
  }

  renderList() {
    return this.props.streams.map(({ id, description, title, userId }: any) => {
      return (
        <div className="item" key={id}>
          <div>{this.renderAdmin(id, userId)}</div>
          <i className="large middle aligned icon camera" />
          <div className="content">
            <Link className="header" to={`/streams/${id}`}>
              {title}
            </Link>
            <div className="description">{description}</div>
          </div>
        </div>
      );
    });
  }

  renderCreate() {
    if (this.props.isSignedIn) {
      return (
        <div style={{ textAlign: "right" }}>
          <Link className="ui button primary" to="/streams/new">
            Create Stream
          </Link>
        </div>
      );
    } else {
      return null;
    }
  }

  render() {
    return (
      <div>
        <h2>Streams</h2>
        <div className="ui celled list">{this.renderList()}</div>
        {this.renderCreate()}
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    streams: Object.values(state.streams),
    currentUserId: state.auth.id,
    isSignedIn: state.auth.isSignedIn,
  };
};

export default connect(mapStateToProps, { fetchStreams })(StreamList);
