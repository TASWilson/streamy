import React from "react";
import { connect } from "react-redux";
import flv from "flv.js";
import { fetchStream } from "../../actions";

class StreamShow extends React.Component<any> {
  videoRef: any;
  player: any;
  constructor(props: any) {
    super(props);
    this.videoRef = React.createRef();
  }

  componentDidMount() {
    const { id } = this.props.match.params;

    this.props.fetchStream(id);
    this.buildPlayer();
  }

  componentDidUpdate() {
    this.buildPlayer();
  }

  buildPlayer() {
    if (this.player || !this.props.stream) {
      return;
    }
    const { id } = this.props.match.params;

    this.player = flv.createPlayer({
      type: "flv",
      url: `http://localhost:8000/live/${id}.flv`,
    });
    this.player.attachMediaElement(this.videoRef.current);
    this.player.load();
  }

  render() {
    if (!this.props.stream) return <div>Loading...</div>;
    const { title, description } = this.props.stream;
    return (
      <div>
        <h1>{title}</h1>
        <h5>{description}</h5>
        <video style={{ width: "100%" }} ref={this.videoRef} controls />
      </div>
    );
  }
}

const componentDidMount = (state: any, ownProps: any) => {
  return { stream: state.streams[ownProps.match.params.id] };
};

export default connect(componentDidMount, { fetchStream })(StreamShow);
