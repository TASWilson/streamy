This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Streamy
This is a demo application create while learning React/Redux. It contains a CRUD application for webcast streams. It uses a dummy json db to for state and contains an rmtp server to recieve stream content. The application was creating using the udemy course [Modern React with Redux](https://www.udemy.com/course/react-redux/).