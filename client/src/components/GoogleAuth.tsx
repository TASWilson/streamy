import React from "react";
import { connect } from "react-redux";

import { signIn, signOut } from "../actions";

interface IGoogleAuthProps {
  signIn(id?: String): any;
  signOut(): any;
  isSignedIn: boolean;
}
interface IGoogleAuthState {}

class GoogleAuth extends React.Component<IGoogleAuthProps, IGoogleAuthState> {
  auth: gapi.auth2.GoogleAuth | null = null;

  componentDidMount() {
    window.gapi.load("client:auth2", () => {
      window.gapi.client
        .init({
          clientId:
            "1021427419234-hs2367reet2jcl4k1p9oj2otfm1rtfd8.apps.googleusercontent.com",
          scope: "email",
        })
        .then(() => {
          this.auth = window.gapi.auth2.getAuthInstance();
          this.onAuthChange(this.auth.isSignedIn.get());
          this.auth.isSignedIn.listen(this.onAuthChange);
        });
    });
  }

  onAuthChange = (isSignedIn: boolean): void => {
    if (isSignedIn) {
      let id;
      if (this.auth) id = this.auth.currentUser.get().getId();
      this.props.signIn(id);
    } else if (!isSignedIn) {
      this.props.signOut();
    }
  };

  handleSignIn = () => {
    this.auth?.signIn();
  };

  handleSignOut = () => {
    this.auth?.signOut();
  };

  renderAuthButton() {
    if (this.props.isSignedIn === null) {
      return null;
    } else if (this.props.isSignedIn) {
      return (
        <button onClick={this.handleSignOut} className="ui red google button">
          <i className="google icon" />
          Sign Out
        </button>
      );
    } else {
      return (
        <button onClick={this.handleSignIn} className="ui red google button">
          <i className="google icon" />
          Sign In with Google
        </button>
      );
    }
  }

  render() {
    return <div>{this.renderAuthButton()} </div>;
  }
}

const mapStateToProps = (state: any) => {
  const props = { isSignedIn: state.auth.isSignedIn };
  return props;
};

export default connect(mapStateToProps, { signIn, signOut })(GoogleAuth);
