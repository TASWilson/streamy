import { SIGN_IN, SIGN_OUT } from '../actions/types'

const INTIAL_STATE = {
    isSignedIn: null,
    id: null
}

export default (state = INTIAL_STATE, action: any) => {
    switch (action.type) {
        case SIGN_IN:
            return { ...state, isSignedIn: true, id: action.payload.id }
        case SIGN_OUT:
            return { ...state, isSignedIn: false, id: null }
        default:
            return state
    }

}